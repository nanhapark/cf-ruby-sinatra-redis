$ vmc logs|more
Using manifest file manifest.yml

[2013-10-06 14:03:10] Setting up temporary directories
[2013-10-06 14:03:10] Downloading application
[2013-10-06 14:03:10] Unpacking application
[2013-10-06 14:03:10] Staging application
[2013-10-06 14:03:19] # Logfile created on 2013-10-06 14:03:10 +0000 by logger.rb/31641
[2013-10-06 14:03:19] Need to fetch redis-3.0.4.gem from RubyGems
[2013-10-06 14:03:19] Adding redis-3.0.4.gem to app...
[2013-10-06 14:03:19] Adding rack-1.5.2.gem to app...
[2013-10-06 14:03:19] Adding rack-protection-1.5.0.gem to app...
[2013-10-06 14:03:19] Adding tilt-1.4.1.gem to app...
[2013-10-06 14:03:19] Adding sinatra-1.4.3.gem to app...
[2013-10-06 14:03:19] Adding bundler-1.1.3.gem to app...
[2013-10-06 14:03:19] Adding cf-autoconfig-0.0.4.gem to app...
[2013-10-06 14:03:19] Adding cf-runtime-0.0.2.gem to app...
[2013-10-06 14:03:19] Creating droplet
[2013-10-06 14:03:19] Uploading droplet
[2013-10-06 14:03:21] Done!


[2013-10-06 14:03:21] INFO  WEBrick 1.3.1
[2013-10-06 14:03:21] INFO  ruby 1.9.3 (2012-04-20) [x86_64-linux]
== Sinatra/1.4.3 has taken the stage on 44855 for production with backup from WEBrick
[2013-10-06 14:03:21] INFO  WEBrick::HTTPServer#start: pid=9939 port=44855


Loading Redis auto-reconfiguration.
No Mongo service bound to app.  Skipping auto-reconfiguration.
No MySQL service bound to app.  Skipping auto-reconfiguration.
No PostgreSQL service bound to app.  Skipping auto-reconfiguration.
No RabbitMQ service bound to app.  Skipping auto-reconfiguration.
Auto-reconfiguring Redis.
